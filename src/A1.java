import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.ie.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class A1 {
	public void kill(String s) throws IOException
	{
	if(s.equals("chrome"))
			 Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe");
		if(s.equals("ie"))
			 Runtime.getRuntime().exec("taskkill /F /IM IEDriverServer.exe");
		if(s.equals("gecko"))
			Runtime.getRuntime().exec("taskkill /F /IM geckodrive.exe");
		if(s.equals("edge"))
			 Runtime.getRuntime().exec("taskkill /F /IM MicrosoftWebDriver.exe");
	} 
	
public static void main(String[] args) throws IOException 
{
		A1 obj=new A1();
//chrome driver
		System.setProperty("webdriver.chrome.driver","D:\\Drivers\\chromedriver.exe");
		WebDriver a=new ChromeDriver();
		a.get("http://shop.demoqa.com/");
		obj.kill("chrome");
//ie driver
		System.setProperty("webdriver.ie.driver","D:\\Drivers\\IEDriverServer_x64_3.14.0\\IEDriverServer.exe");
		WebDriver b = new InternetExplorerDriver();
		b.get("http://shop.demoqa.com/");
		obj.kill("ie");
//firefox
		System.setProperty("webdriver.gecko.driver","D:\\Drivers\\geckodriver-v0.26.0-win64\\geckodriver.exe");
		WebDriver c = new FirefoxDriver();
		c.get("http://shop.demoqa.com/");
		obj.kill("gecko");
//edge
		System.setProperty("webdriver.edge.driver","D:\\Drivers\\MicrosoftWebDriver.exe");
		WebDriver d = new EdgeDriver();
		d.get("http://shop.demoqa.com/");
//headless
		
//		System.setProperty("webdriver.edge.driver","D:\\Drivers\\htmlunit-driver-2.36.0-jar-with-dependencies.jar");
//		WebDriver dr = new HtmlUnitDriver();
//		dr.get("http://shop.demoqa.com/");
		
		
		
		
		obj.kill("edge");
	}

}




